/* 

APPLICATION PROGRAMMING INTERFEACE (API)

An application programming interface (API) is a connection between computers or between computer programs. It is a type of software interface, offering a service to other pieces of software.

*/

// const mainContainer = document.getElementById("main");

// mainContainer.innerHTML = "hello"




















/* 

SYNCHRONOUS CODE

Synchronous refers to real-time communication where each party receives (and if necessary, processes and replies to) messages instantly (or as near to instantly as possible).

ASYNCHRONOUS CODE 

The term asynchronous refers to two or more objects or events not existing or happening at the same time (or multiple related things happening without waiting for the previous one to complete).

*/

// console.log("first")

// console.log("second");

// console.log("third")


//some code that gets information from another location -> time depende

// console.log("first");

// setTimeout(() => {
//     console.log("second")
// }, 2000)

// console.log("thrid");















/* 

FETCH API

The Fetch API provides an interface for fetching resources (including across the network).

The global fetch() method starts the process of fetching a resource from the network, returning a promise which is fulfilled once the response is available.

Syntax:
fetch(url, options)

url - where you are requesting a certian data from;
options - information regarding the request
    options = {
        headers,
        method: GET,
        body
    }

A fetch request is an asynchronous operation. 

fetch returns a PROMISE;

*/










/* 

PROMISE 

The Promise object represents the eventual completion (or failure) of an asynchronous operation and its resulting value.

A Promise is a proxy for a value not necessarily known when the promise is created. It allows you to associate handlers with an asynchronous action's eventual success value or failure reason. This lets asynchronous methods return values like synchronous methods: instead of immediately returning the final value, the asynchronous method returns a promise to supply the value at some point in the future.

A Promise is in one of these states:

pending: initial state, neither fulfilled nor rejected.

fulfilled: meaning that the operation was completed successfully.

rejected: meaning that the operation failed.

syntax:

const myPromise = new Promise((resolve, reject) => {
    if(some condition) resolve("fullfilled");
    else reject("Rejected");
})

*/




// const myNum = 2;

// const myPromise = new Promise((reslove, reject) => {
//     if(myNum === 2) reslove("It's two");
//     else reject("It's not two");
// });


// async function myFunc(){
//     try {
//         const response = await fetch("https://jsonplaceholder.typicode.com/posts");
//         const data = await response.json();
//         console.log(data)
//     } catch (error) {
//         console.log(error)
//     }
// }

// console.log("before")

// myFunc();

// console.log("after")

// const num = 2;

// console.log("before")

// try {
//     num = 3;
// } catch (error) {
//     console.log(error.message)
// }


// console.log("after")



// myPromise
// .then(response => console.log(response))
// .catch(error => console.log(error))

// fetch("https://jsonplaceholder.typicode.com/posts")
// .then(response => response.json())
// .then(data => console.log(data))
// .catch(error => console.log(error))






/*

Promise.then()

The then() method returns a Promise. It takes up to two arguments: callback functions for the success and failure cases of the Promise.

It's usually used when the promise returns a fullfilled state. 

Promise.catch()

The catch() method returns a Promise and deals with rejected cases only.

THROW 

The throw statement throws a user-defined exception. Execution of the current function will stop (the statements after throw won't be executed), and control will be passed to the first catch block in the call stack. If no catch block exists among caller functions, the program will terminate.

syntax: 

throw "some error statement"

*/




/* 

ASYNC / AWAIT FUNCTIONS

An async function is a function declared with the async keyword, and the await keyword is permitted within them. The async and await keywords enable asynchronous, promise-based behavior to be written in a cleaner style, avoiding the need to explicitly configure promise chains.

The await expression causes async function execution to pause until a Promise is settled (that is, fulfilled or rejected), and to resume execution of the async function after fulfillment. When resumed, the value of the await expression is that of the fulfilled Promise.

syntax:

async function myFunc() {
    const response = await fetch(something);
    // do something with the response 
}

const myFunc = async () => {
    const response = await fetch(something);
    // do something with the response 
}

*/





/* 

try...catch

The try...catch statement marks a block of statements to try and specifies a response should an exception be thrown.

They allow the flow of code to be executed even if it encounters some error in between the code. Given that all the code required to be 'tried' is confined to the try block;

*/