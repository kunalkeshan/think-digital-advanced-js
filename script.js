const POSTS = "https://jsonplaceholder.typicode.com/posts";
const PHOTOS = "https://jsonplaceholder.typicode.com/photos";

const mainContainer = document.getElementById("main");

const createCard = ({url, title, body}) => {
    const card = `
    <div class="card">
        <img src=${url}>
        <div class="card__info">
            <p class="card__cta flex">
                <i class="fas fa-heart heart"></i>
                <i class="fas fa-comments comment"></i>
                <i class="fas fa-share-alt share"></i>
            </p>
            <p class="card__title">${title}</p>
            <p class="card__description">${body}</p>
        </div>
    </div>
    `
    return card;
}

const updateBody = (element, root) => {
    root.innerHTML += element;
}

const fetchData = async(URL) => {
    try {
        const response = await fetch(URL);
        const data = await response.json();
        return data;
    } catch (error) {
        console.log(error);
    }
}

const mergeData = async () => {
    try {
        const posts = await fetchData(POSTS);
        const photos = await fetchData(PHOTOS);
        const postsCollection = posts.map((post, index) => {
            post.url = photos[index].url;
            return post;
        });
        return postsCollection;
        
    } catch (error) {
        console.log(error);
    }
}

mergeData()
.then(data => {
    data.forEach(element => {
        updateBody(createCard(element), mainContainer)
    });
})
.catch(() => console.log("Fetch error"))